Feature: Vehiculos

    Scenario: Registro de un vehiculo
        Given I wait to see "Vehicles"
        And I press "Vehicles"
        And I take a screenshot
        Then I press the menu key
        And I take a screenshot
        And I press "Add new vehicle"
        And I take a screenshot
        Then I enter "DUKE 200" into input field number 1
        Then I enter "2016" into input field number 2
        Then I enter "KTM" into input field number 3
        Then I enter "2016" into input field number 4
        And I press "Add new vehicle"
        And I take a screenshot
        Then I should see "Vehicles"
        Then I should see "DUKE 200"

    Scenario: Detallar un vehiculo
        Given I wait to see "Vehicles"
        And I press "Vehicles"
        And I take a screenshot
        And I press "DUKE 200"
        Then I should see "Vehicles"
        And I take a screenshot
        Then I should see "DUKE 200"
Feature: Consumos

  Scenario: Registro de un consumo
    Given I wait to see "Fillup"
    Then I enter "10000" into input field number 1
    And I take a screenshot
    Then I enter "2" into input field number 2
    Then I enter "200" into input field number 3
    And I press "Save Fillup"
    Then I should see "10,000.00"
    And I take a screenshot

  Scenario: Ver detalle de un consumo
    Given I wait to see "Fillup"
    When I press "History"
    And I take a screenshot
    And I press "10,000.00"
    And I take a screenshot
    Then I should see "200.00"
    And I should see "$10000.00"
    And I should see "$20000.00"
    And I take a screenshot
    And I go back


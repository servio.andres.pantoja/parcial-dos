Feature: Estadisticas

  Scenario: Ver estadisticas
    Given I wait to see "Fillup"
    Then I enter "10000" into input field number 1
    And I take a screenshot
    Then I enter "2" into input field number 2
    Then I enter "200" into input field number 3
    And I press "Save Fillup"
    And I take a screenshot
    When I press "Fillup"
    And I take a screenshot
    Then I enter "15000" into input field number 1
    Then I enter "2" into input field number 2
    Then I enter "200" into input field number 3
    And I press "Save Fillup"
    And I take a screenshot
    Given I wait to see "Statistics"
    And I take a screenshot
    When I press "Statistics"
    And I take a screenshot
    Given I wait to see "25000.00"
    Given I wait to see "12500.00"